import HandlerRequest from "../interfaces/HandlerRequest.interface";
import DialogflowHandler from "../handlers/Dialogflow.handler";


class AppSenderController {

  /**
   * webhook for skype and teams
   * @param {*} req 
   * @param {*} res 
   */
  static async webhookMicrosoft(req:any, res:any) {
    try {
	  console.dir(req.body, {depth: null});
	  if(req.body.type!='message'){
		  res.send(200);
	  }
      let request: HandlerRequest = {
                chanelId: req.body.from.id,
                chanelName: req.body.channelId,
                usermsg: req.body.text?.replace("&apos;", "'"),
                intent: '',
                attachments:req.body.attachments,
                req: req,
                res: res
            };
            DialogflowHandler.getInstance().handle(request);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
  // static async webhook(req:any, res:any){
  //   if (req.body.channelId == "msteams" || req.body.channelId == "skype") {
  //     AppSenderController.webhookMicrosoft(req, res);
  //   } 
  // }
}

export default AppSenderController;