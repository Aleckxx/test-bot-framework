import { Router } from 'express';
import AppSenderController from '../controllers/appSender.controller';

const router = Router();

router.post('/', AppSenderController.webhookMicrosoft);
router.get('/',(req:any, res: any) =>{
	res.send(200);
});
router.get('/log',(req:any, res: any) =>{
	const file = `/app/logs/log.log`;
	res.download(file);
});
export default router;
