import { Request, Response } from "express";

export default interface HandlerRequest{
    dialogflowresponse?: any;
    intent?: string | null;
    chanelId: string;
    chanelName: string;
    message?: any;
    usermsg?: string;
    req?: Request;
    res?: Response;
    attachments?:any;
    passRuleEngine?: boolean;
    accountId?: any;
    action?: any;
    actionIssue?: any;
    saveOrder?: any;
}