import HandlerRequest from "./HandlerRequest.interface";

export default interface Handler{
    nextHandler?: Handler;
    handle(request:HandlerRequest): any;
}