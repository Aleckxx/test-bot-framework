import { TurnContext } from "botbuilder";
import {  connector } from "../../global_variable";
import HandlerRequest from "../interfaces/HandlerRequest.interface";
import AbstractHandler from "./Abstract.handler";


export default class SenderHandler extends AbstractHandler{
    static instance: SenderHandler;
    constructor(){
        super();
    }
    static getInstance(){
        if(!SenderHandler.instance){
            SenderHandler.instance = new SenderHandler();
        }
        return this.instance;
    }
    async handle(request: HandlerRequest){
        console.log("Sender handler entered");
        var message = request.message;
        if(message){
            await this.sendResponseMessage(request);
        }
    }
    async sendResponseMessage(request: HandlerRequest){
        var req:any = request.req;
        var res:any = request.res;
        var message:any = request.message;
        if(request.chanelName=='msteams'||request.chanelName=='skype'){
            await connector.processActivity(req, res, async (context: TurnContext) => {
				var date1:Date = new Date();
                //bot.addConversationReference(context.activity);
                await context.sendActivity(message);
				var date2:Date = new Date();
				var diff = date2.getTime()-date1.getTime();
				console.log("message sent: "+diff);
            });
        }
    }
}