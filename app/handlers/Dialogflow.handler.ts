import AbstractHandler from "./Abstract.handler";
import SenderHandler from "./Sender.handler";
import DialogflowConfig from "../services/DialogflowConfig.service";
import HandlerRequest from "../interfaces/HandlerRequest.interface";

export default class DialogflowHandler extends AbstractHandler{
    static instance: DialogflowHandler;
    static getInstance(){
        if(!DialogflowHandler.instance){
            DialogflowHandler.instance = new DialogflowHandler();
        }
        return DialogflowHandler.instance;
    }
    constructor(){
        super();
        this.setNext(SenderHandler.getInstance());
    }
    async handle(request: HandlerRequest){
        var dialogflowresponse = await DialogflowConfig.useDialogflow(request.usermsg, request.chanelId);
        //console.log(util.inspect(dialogflowresponse,{showHidden: false, depth: null, colors: true}));
        request.dialogflowresponse = dialogflowresponse[0];
        request.intent = dialogflowresponse[0].queryResult.intent.displayName;
        request.message = dialogflowresponse[0].queryResult.fulfillmentText;
        super.handle(request);
    }
}