import Handler from "../interfaces/Handler.interface"
import HandlerRequest from "../interfaces/HandlerRequest.interface";

export default class AbstractHandler implements Handler{
    nextHandler?: Handler;
    async handle(request: HandlerRequest){
        if(this.nextHandler){
            this.nextHandler.handle(request);
        }
    }
    setNext(handler: Handler){
        this.nextHandler = handler;
    }
}