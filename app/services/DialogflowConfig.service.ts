

require('dotenv').config();
const { struct } = require("pb-util");

class DialogflowConfig {
    static async useDialogflow(message: string|undefined, idClient: any) {
        idClient = idClient.replace("/", "");
        const keyPath = process.env.SESSIONCLIENT;
        if (!keyPath) {
            process.exit(1);
        }
        const projectId = process.env.PROJECTIDDF;
        if (!projectId) {
            process.exit(1);
        }
        // // Create a new session
        const dialogflow = require('@google-cloud/dialogflow');
		var date1:Date = new Date();
        const sessionClient = new dialogflow.SessionsClient({ keyFilename: keyPath });
        const sessionPath = sessionClient.projectAgentSessionPath(projectId, (idClient));
		var date2:Date = new Date();
		var diff = date2.getTime()-date1.getTime();
		console.log("session client: "+diff);
        // // The text query request.
        const request = {
            session: sessionPath,
            queryInput: {
                text: {
                    text: message,
                    languageCode: 'fr',
                },
            },
        };
		date1 = new Date();
        // // Send request and log result
        const responses = await sessionClient.detectIntent(request);
		date2 = new Date();
		diff = date2.getTime()-date1.getTime();
		console.log("detect intent: "+diff);
        return responses;
    }
}
export default DialogflowConfig;