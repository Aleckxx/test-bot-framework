import express from 'express';
import * as http from 'http';
import * as winston from 'winston';
import * as expressWinston from 'express-winston';
import cors from 'cors';
import debug from 'debug';

// Chargez nos classes personnalisées

import webhookRoute from './app/routes/webhook.route';

/*Change environnement Local et recette */
require('dotenv').config();
require('custom-env').env('local');


// Charger les dépendances tierces
var cookieParser = require('cookie-parser');
const app: any = express();
const server: http.Server = http.createServer(app);
const io = require('socket.io')(server);

let logger = require('morgan');
let path = require('path');
var rfs = require('rotating-file-stream');

const keyPath = process.env.SESSIONCLIENT;
const projectId = process.env.PROJECTIDDF;

//charger passport pour l'authentication
const passport = require('passport');
const session = require('express-session');
const bodyParser = require('body-parser');
var LocalStrategy = require('passport-local').Strategy;


//configurer session middleware
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));
app.use(session({
  secret: 'ABCDEFGAERTV/;,&',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60 * 60 * 1000, secure: false } // 1 hour
}));
passport.serializeUser((user:any,done:any)=>{
  done(null,user.login);
});


passport.deserializeUser((email:any,done:any)=>{
 try{
  done (null,{});
 }
 catch(err){
    console.log(err);
    done(err);
 }
});
passport.use(new LocalStrategy(
async (username:string,password:string,done:any)=>{
  try{
      let user = {};
      return done(null,user);
  }
  catch(err){
    let tempo:any = err;
    return done(null,false,tempo.message);
  }
}
));


// Configure Middleware
app.use(passport.initialize());
app.use(passport.session());

// Chargez nos classes personnalisées

if (!keyPath) {
  process.exit(1);
}

if (!projectId) {
  process.exit(1);
}

//app.use(express.json({}));
//app.use(cookieParser());
// Add headers before Listen
app.use(function (req:any, res:any, next:any) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  // Pass to next layer of middleware
  next();
});


// here we are adding middleware to allow cross-origin requests
app.use(cors());

const port = process.env.PORT || 8000;
process.setMaxListeners(0);

// view engine setup


// here we are adding middleware to parse all incoming requests as JSON 
//app.use(express.json());
//app.use(express.urlencoded({ extended: false }));

//Setup morgan logger
if (process.env.URL === "") {
  app.use(logger(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :response-time ms :res[content-length] ":referrer"'));
} else {
  //Crée un fichier de log par jour
  var accessLogStream = rfs.createStream('access.log', {
    interval: '1d',
    path: path.join(__dirname, './logs')
  });
  app.use(logger(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :response-time ms :res[content-length] ":referrer"', {
    stream: accessLogStream
  }));
  
}

// Declaration routes


const debugLog: debug.IDebugger = debug('app');

// here we are preparing the expressWinston logging middleware configuration,
// which will automatically log all HTTP requests handled by Express.js
const loggerOptions: expressWinston.LoggerOptions = {
  transports: [new winston.transports.Console(),new winston.transports.File({
            filename: 'logs/logging.log'
        })],
  format: winston.format.combine(
    winston.format.json(),
    winston.format.prettyPrint(),
    winston.format.colorize({ all: true })
  ),
};

if (!process.env.DEBUG) {
  loggerOptions.meta = false; // when not debugging, log requests as one-liners
}

var winlog = expressWinston.logger(loggerOptions);
// initialize the logger with the above configuration
app.use(winlog);

// here we are adding the UserRoutes to our array,
// after sending the Express.js application object to have the routes added to our app!

app.use('/',webhookRoute);

server.listen(port, () => {
  
  // our only exception to avoiding console.log(), because we
  // always want to know when the server is done starting up
  console.log(`Server and socket are running on PORT ${port}`);
});
