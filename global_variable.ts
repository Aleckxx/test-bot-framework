
import { BotFrameworkAdapter } from "botbuilder";
require("dotenv").config();
export var connector:BotFrameworkAdapter = new BotFrameworkAdapter({
    appId: process.env.MS_APP_ID,
    appPassword: process.env.MS_APP_PASSWORD
});